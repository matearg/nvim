-- Floaterm configs
vim.g.floaterm_shell= '"C:\\Users\\mateo\\AppData\\Local\\Microsoft\\WindowsApps\\Microsoft.PowerShell_8wekyb3d8bbwe\\pwsh.exe" -nologo'
vim.g.floaterm_title = "Terminal"
vim.g.floaterm_borderchars = "─│─│┌┐┘└" -- Avalible "─│─│╭╮╯╰", "─│─│┌┐┘└", "═║═║╔╗╝╚"
